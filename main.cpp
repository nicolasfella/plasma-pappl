/*
 * SPDX-FileCopyrightText: 2020 Nicolas Fella <nicolas.fella@gmx.de>
 * SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
 */
#include <pappl/pappl.h>

#include <QCoreApplication>
#include <QDebug>

#include <memory>

void destroy(pappl_system_t *s)
{
    papplSystemDelete(s);
}

int main(int argc, char **argv)
{
    QCoreApplication app(argc, argv);

    std::unique_ptr<pappl_system_t, decltype(&destroy)> sys(papplSystemCreate(PAPPL_SOPTIONS_NONE, "hello", 0, nullptr, nullptr, nullptr, PAPPL_LOGLEVEL_DEBUG, nullptr, false), destroy);

    qDebug() << "hello" << sys.get();

    return app.exec();
}
